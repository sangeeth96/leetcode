# Time: O(k · m · n)
# Space: O(1)
class Solution:
    def shiftGrid(self, grid: List[List[int]], k: int) -> List[List[int]]:
        # Simulation approach, we shift the 2D grid one time, k times
        # This is acceptable for low values of k (given k <= 100)
        rows, cols = len(grid), len(grid[0])
        
        for _ in range(k):
            # Assume this is the first shift. When we finish shifting all
            # elements, the first item in the resulting grid will be the
            # last item from the original grid.
            prev = grid[-1][-1]
            
            for i in range(rows):
                for j in range(cols):
                    tmp = grid[i][j]
                    grid[i][j] = prev
                    prev = tmp
        
        return grid
