# Time complexity: O(nlogn)
# https://github.com/python/cpython/blob/f52d987abfda25e50469c9b6fe1d19f72453d2de/Lib/collections/__init__.py#L608
from collections import Counter

class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        return [val for val, _ in Counter(nums).most_common(k)]
