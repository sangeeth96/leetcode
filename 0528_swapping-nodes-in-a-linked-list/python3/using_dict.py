# NOTE: Not the best approach, since we use O(N) space
# Time: O(N)
class Solution:
    def swapNodes(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        # Figure out length of LL and cache nodes by the index
        length = 0
        cache = {}
        
        ptr = head
        while ptr != None:
            cache[length] = ptr
            length += 1
            ptr = ptr.next
        
        cache[k - 1].val, cache[length - k].val = cache[length - k].val, cache[k - 1].val
        
        return head
