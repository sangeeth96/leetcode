# Time: O(N)
# Space: O(N)
class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        n = len(nums)
        result = [None] * n
        
        for i, num in enumerate(nums):
            new_i = (i + k) % n
            result[new_i] = num
            
        for i, num in enumerate(nums):
            nums[i] = result[i]
