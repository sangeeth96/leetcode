# Time: O(N)
# Space: O(N)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    maxDepthValue = 0
    
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        '''
        Recursive DFS
        '''
        return 0 if root is None else 1 + max(self.maxDepth(root.left), self.maxDepth(root.right))
