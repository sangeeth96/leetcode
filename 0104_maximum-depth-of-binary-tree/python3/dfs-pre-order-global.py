# Time: O(N)
# Space: O(N)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    maxDepthValue = 0
    
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        '''
        Recursive DFS pre-order but storing max globally
        '''
        def findMax(node, depth):
            if node is None: return None
            
            currDepth = depth + 1
            self.maxDepthValue = max(currDepth, self.maxDepthValue)
            findMax(node.left, currDepth)
            findMax(node.right, currDepth)
        
        findMax(root, 0)
        
        return self.maxDepthValue
