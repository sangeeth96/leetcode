class Solution:
    def sortArrayByParity(self, nums: List[int]) -> List[int]:
        ins_pos, next_pos = 0, 0
        
        while ins_pos < len(nums) and next_pos < len(nums):
            if nums[ins_pos] & 1 == 0:
                ins_pos += 1
                next_pos = ins_pos
                continue
            
            if nums[next_pos] & 1 == 0:
                nums[ins_pos], nums[next_pos] = nums[next_pos], nums[ins_pos]
                ins_pos += 1
                
            next_pos += 1
        
        return nums
