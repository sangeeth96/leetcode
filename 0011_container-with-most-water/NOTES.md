## Approaches:

1. Brute force $O(N^2)$
2. Two-pointer $O(N)$

## Follow-up questions

1. What if you could slant the container?
