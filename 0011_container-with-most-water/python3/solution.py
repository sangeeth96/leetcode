class Solution:
    def maxArea(self, height: List[int]) -> int:
        left = 0
        right = len(height) - 1
        
        max_area = float("-inf")
        while left < right:
            if height[left] <= height[right]:
                area = height[left] * (right - left)
                max_area = max(max_area, area)
                left += 1
            else:
                area = height[right] * (right - left)
                max_area = max(max_area, area)
                right -= 1
        
        return max_area
