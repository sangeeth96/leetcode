func isSubsequence(s string, t string) bool {
	slen, tlen, si, ti := len(s), len(t), 0, 0

	if slen == 0 {
		return true
	}

	if slen > tlen {
		return false
	}

	for ti < tlen && si < slen {
		if s[si] == t[ti] {
			si += 1
		}

		ti += 1
	}

	return si == slen
}
