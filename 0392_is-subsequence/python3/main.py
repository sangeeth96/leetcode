class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        slen, tlen, si, ti = len(s), len(t), 0, 0
        
        # If s is longer than t, it's obviously not
        # going to be a subsequence
        if slen > tlen:
            return False
        
        while ti < tlen and si < slen:
            if s[si] == t[ti]:
                si += 1
            
            ti += 1

        return si == slen
