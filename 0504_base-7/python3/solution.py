class Solution:
    def convertToBase7(self, num: int) -> str:
        sign = '-' if num < 0 else ''
        num = abs(num)
        b7 = ''
        
        while num > 0:
            b7 = str(num % 7) + b7
            num //= 7
        
        return '0' if b7 == '' else sign + b7
