## What if we were allowed to use division?

Well, one might think that you can then find product(nums) and divide by nums[i] in the loop. But the problem with this approach is when there are 0s in the array. That causes the entire product to become zero.

So, we probably need to keep track of zero count and the first zero index.

1. If there are >1 zeroes, then the result would be all zeroes so.
2. If there's only 1 zero, then we need to keep product excluding the zero to be kept. Then fill the result with all zeroes except for the 0 element's index which would equal the product.
