class Solution:
    def sortedSquares(self, nums: List[int]) -> List[int]:
        left = 0
        right = i = len(nums) - 1
        result = [None] * len(nums)
        
        while i >= 0:
            if abs(nums[left]) > abs(nums[right]):
                result[i] = nums[left] ** 2
                left += 1
            else:
                result[i] = nums[right] ** 2
                right -= 1
            
            i -= 1
        
        return result
