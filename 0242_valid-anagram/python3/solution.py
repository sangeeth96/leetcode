# Time: O(N) where N is num of chars in string
# Space: O(N) where N is num of chars in string

from collections import Counter

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        slen, tlen = len(s), len(t)
        
        if slen != tlen:
            return False
        
        scounts, tcounts = {}, {}
        
        for i in range(slen):
            sc, tc = s[i], t[i]
            
            scounts[sc] = scounts.get(sc, 0) + 1
            tcounts[tc] = tcounts.get(tc, 0) + 1
            
        return scounts == tcounts
