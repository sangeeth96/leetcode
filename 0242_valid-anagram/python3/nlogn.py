# Time: O(NlogN)
# Space: O(1)

from collections import Counter

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        slen, tlen = len(s), len(t)
        
        if slen != tlen:
            return False
        
        return sorted(s) == sorted(t)
