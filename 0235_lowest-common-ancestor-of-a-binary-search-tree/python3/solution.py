# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        curr = root
        
        while curr:
            # Both p and q can be found in right subtree so move
            # curr there
            if p.val > curr.val and q.val > curr.val:
                curr = curr.right
            # Both p and q can be found in left subtree so move
            # curr there
            elif p.val < curr.val and q.val < curr.val:
                curr = curr.left
            # If above cases fail, that means we are already at a `curr`
            # where p exists in one side and q exists on another side so 
            # that must mean `curr` is the LCA
            else:
                return curr
