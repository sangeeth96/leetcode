# Time: O(N)
# Space: O(N)

class Solution:
    def trap(self, heights: List[int]) -> int:
        if len(heights) < 2: return 0
        
        # For every ith height, we need to calculate the max height
        # on left and max on right of it
        max_lefts, max_rights = [0] * len(heights), [0] * len(heights)
        
        maxl = 0
        for i in range(len(heights)):
            max_lefts[i] = maxl
            maxl = max(maxl, heights[i])
        
        maxr = 0
        for j in range(len(heights) - 1, -1, -1):
            max_rights[j] = maxr
            maxr = max(maxr, heights[j])
            
        print(max_lefts)
        print(max_rights)
        
        # For every ith height, we can now compute the water output it can hold
        # by doing the following:
        #
        # min(max_height_to_left, max_height_to_right) - height_of_bar
        #
        # If we ignore height_of_bar for a moment, the amount of water that can be
        # held would be constrained by the least heights out of left/right.
        #
        #     3
        # 2   |
        # |   |
        # | 0 |
        #
        # In the above case, would be min(2, 3) == 2. But, if we fill the space up with a bar
        # then:
        #
        #     3
        # 2   |
        # | 1 |
        # | | |
        #
        # We can't put 2 water units. We need to subtract the height of the bar from the prev
        # min() which will give us 1.
        #
        # NOTE: We need to do this for every ith height in the array.
        
        output = 0
        for i, h in enumerate(heights):
            value = min(max_lefts[i], max_rights[i]) - heights[i]
            
            # It's possible that ith height is very large, causing
            # the expression above to give us -ve value. In this case
            # we just ignore it (i.e, 0 output for this ith height)
            if value > 0:
                output += value
        
        return output
