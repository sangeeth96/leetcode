Given a string array representing an expression, and return the Reverse Polish notation of this expression. (remove the parentheses)

Definition of _Reverse Polish Notation_:

* [https://en.wikipedia.org/wiki/Reverse\_Polish\_notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation)

Example

Example 1:
    
        Input: ["3", "-", "4", "+", "5"] Output: ["3", "4", "-", "5", "+"] Explanation: 3 - 4 + 5 = -1 + 5 = 4 3 4 - 5 + = -1 5 + = 4
    
    

Example 2:
    
        Input: ["(", "5", "-", "6", ")", "*", "7"] Output: ["5","6","-","7","*"] Explanation: (5 - 6) * 7 = -1 * 7 = -7 5 6 - 7 * = -1 7 * = -7
