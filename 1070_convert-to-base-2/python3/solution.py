class Solution:
    def baseNeg2(self, n: int) -> str:
        result = ''
        
        while n != 0:
            rem = n % -2
            n = n // -2
            
            if rem < 0:
                n += 1
                rem += 2
            
            result = str(rem) + result
        
        return result if result != '' else '0'
