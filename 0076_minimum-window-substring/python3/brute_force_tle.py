from collections import Counter

class Solution:
    def minWindow(self, s: str, t: str) -> str:
        tcounts = Counter(t)
        
        # If chars of t is not included in the entire
        # s string, then we can just return right away
        if not tcounts <= Counter(s):
            return ""
        
        tlen = len(t)
        min_window = ""
        
        for start in range(len(s)):
            end = start + tlen - 1
            
            # We can all the substrings starting from `start` which is every
            # letter of s
            while end < len(s):
                curr_window = s[start:end + 1]
                curr_window_counts = Counter(curr_window)
                
                # <= is overloaded for Counters to mean inclusion
                #
                # i.e, counter_a -is-included-in- counter_b
                #
                if tcounts <= curr_window_counts:
                    # If no min_window has been computed yet or if current
                    # window is shorter, then we assign that as the new
                    # min_window
                    if min_window == "" or len(curr_window) < len(min_window):
                        min_window = curr_window
                        
                    # Since at this point, if we keep iterating in the inner loop,
                    # we'll only get longer substrings so we just break and proceed
                    # to find the substrings starting from the next letter (start + 1)
                    break

                end += 1
        
        return min_window
