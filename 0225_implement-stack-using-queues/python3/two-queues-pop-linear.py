from collections import deque

class MyStack:

    def __init__(self):
        self.q = deque()
        self.tmp = deque()

    def push(self, x: int) -> None:
        self.q.append(x)
        self.head = x

    def pop(self) -> int:
        tmp = deque()
        popped = None
        prev = None
        
        while True:
            popped = self.q.popleft()
            
            if not self.q:
                break
            
            prev = popped
            tmp.append(prev)
            
        self.q = tmp
        self.head = prev
        
        return popped
        

    def top(self) -> int:
        return self.head

    def empty(self) -> bool:
        return len(self.q) == 0


# Your MyStack object will be instantiated and called as such:
# obj = MyStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.empty()
