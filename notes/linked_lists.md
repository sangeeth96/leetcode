---
category: ds
title: Linked Lists
---

# Linked Lists

## Finding nth node from beginning and nth node from end

Use two pointer approach to avoid extra space. Initialize the `kth_node_from_end` pointer when the `tmp` pointer reaches `kth` node and keep incrementing from there on.

### Related problems

- https://leetcode.com/problems/swapping-nodes-in-a-linked-list
