# Time: O(N * MlogM) where N is len(strs) and M is num of chars in a string
# Space: O(N)

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        groups = {}
        
        for s in strs:
            s_sorted = ''.join(sorted(s))
            
            groups[s_sorted] = groups.get(s_sorted, []) + [s]
        
        return list(groups.values())
