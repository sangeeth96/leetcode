import { request, GraphQLClient, gql } from "graphql-request";

const customFetch = require("fetch-cookie")(fetch);
export const client = new GraphQLClient("https://leetcode.com/graphql", {
  headers: {
    Referer: "https://leetcode.com",
  },
  fetch: customFetch,
});

const lastFetch = await customFetch("https://leetcode.com", {
  redirect: "follow",
  headers: {
    Referer: "https://leetcode.com",
  },
});

client.setHeader(
  "x-csrftoken",
  /csrftoken=([^;]+)/.exec(lastFetch.headers.get("set-cookie"))[1]
);

export { gql };
