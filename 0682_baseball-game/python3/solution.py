from collections import deque

class Solution:
    def calPoints(self, ops: List[str]) -> int:
        d = deque()
        
        for _, op in enumerate(ops):
            if op == "C":
                d.pop()
            elif op == "D":
                d.append(d[-1] * 2)
            elif op == "+":
                d.append(d[-1] + d[-2])
            else:
                d.append(int(op))
        
        total = 0
        while len(d) > 0:
            total += d.pop()
        
        return total
