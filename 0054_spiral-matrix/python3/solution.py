class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        row_begin_loc, row_end_loc = 0, len(matrix) - 1
        col_begin_loc, col_end_loc = 0, len(matrix[0]) - 1
        result = []
        
        while row_begin_loc <= row_end_loc and col_begin_loc <= col_end_loc:
            # Top Side
            i, j = row_begin_loc, col_begin_loc
            while j <= col_end_loc:
                result.append(matrix[i][j])
                j += 1

            # Right Side
            i += 1
            j -= 1
            while i <= row_end_loc:
                result.append(matrix[i][j])
                i += 1

            # Bottom Side
            i -= 1
            j -= 1
            while j >= col_begin_loc and i > row_begin_loc:
                result.append(matrix[i][j])
                j -= 1
            
            # Left Side
            i -= 1
            j += 1
            while i > row_begin_loc and j < col_end_loc:
                result.append(matrix[i][j])
                i -= 1
            
            row_begin_loc += 1
            row_end_loc -= 1
            col_begin_loc += 1
            col_end_loc -= 1
        
        return result
        